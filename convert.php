<?php

require __DIR__.'/vendor/autoload.php';

use voku\helper\UTF8;

$filePath = $argv[1];
file_exists($filePath) or die('file doesnt exist');

$testId = (int) ($argv[2] ?? 0);
$allTests = $argv[2] === 'all';
$testId or $allTests or die('need to pass test id or "all" parameter');

$handle = fopen($filePath, 'r');

const PAYMENT = [
    'jährlich' => 12,
    'monatlich' => 1,
    'halbjährlich' => 6,
    'quartalsweise' => 3,
];

$json = [];
while (($data = fgetcsv($handle, 1000, ';')) !== FALSE) {
    $id = (int)($data[0] ?? 0);
    if (!$allTests) {
        if ($id !== $testId) {
            continue;
        }
    }

    if ($id === 0) {
        continue;
    }

    $extractValue = function ($value) { return substr($value, strpos($value, ':') + 1); } ;

    $extractKey = function ($value) { return substr($value, 0, strpos($value, ':')); } ;

    $row = [];
    $row['insurer_profession'] = UTF8::fix_utf8($extractValue($data[3]));
    $row['gv24_profession'] = UTF8::fix_utf8(((int)substr($data[1], strpos($data[1], ':') + 1)) < 0 ? '' : substr($data[1], strpos($data[1], ':') + 1));
    $row['premium'] = (float)str_replace(',', '.', substr($data[2], strpos($data[2], ':') + 1));
    $row['loop_conditions'] =  [
        'tariff_name' => str_replace(['-', ' ', ','], '', mb_strtolower($extractValue($data[4]))),
        'duration' => (float)$extractValue($data[8]),
        'insurance_sum' => (float)$extractValue($data[5]),
        'deductible' => (float)$extractValue($data[6]),
        'payment_frequency' => PAYMENT[(int)$extractValue($data[7])] ?? (int)$extractValue($data[7]),
    ];

    $row['questions'] = [];
    for ($i = 9; $i < count($data); $i++) {
        $key = $extractKey($data[$i]);
        if (strpos($key, '+') !== false || strpos($key, 'or') !== false || strpos($key, '-') !== false)
            continue;

        $intKey = (int)$key;
        if( $intKey == 0 || $intKey != $key ) {
            continue;
        }

        $row['questions'][$key] = ['answers' => [UTF8::fix_utf8($extractValue($data[$i]))]];
    }
    ksort($row['questions']);
    ksort($row);

    $json['test_' . $id] = $row;
}

$jsonString = json_encode($json, JSON_UNESCAPED_UNICODE);

echo $jsonString;

if ($jsonString == false) {
    echo json_last_error_msg();
}




